FROM zachpodbielniak/crock:latest

LABEL maintainer="Zach Podbielniak"

WORKDIR /gpia
COPY . .

RUN make prod

EXPOSE 9998 
CMD ["/gpia/bin/gpia"]
