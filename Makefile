bin: 
	mkdir -p bin/

clean: 
	rm -rf bin/

prod: bin
	gcc -o bin/gpia Src/Main.c -lpodnet -lcrock -lpthread 

docker: 
	docker build -t zachpodbielniak/gpia:latest .

push:
	docker push zachpodbielniak/gpia:latest

