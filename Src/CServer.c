#include <PodNet/Ascii.h>
#include <PodNet/CNetworking/CIpv4.h>
#include <PodNet/CNetworking/CIpv6.h>
#include <PodNet/CNetworking/CSocket4.h>
#include <PodNet/CNetworking/CSocket6.h>


#define SERVER_IPV4_PORT 		4444
#define SERVER_IPV6_PORT		6666



typedef enum __REQUEST 
{
	REQUEST_IPV4_ADDRESS 		= ASCII_4,
	REQUEST_IPV6_ADDRESS 		= ASCII_6,
	REQUEST_CLOSE_CONNECTION 	= ASCII_Q_LOWER,
	REQUEST_LICENSE 		= ASCII_L_LOWER,
	REQUEST_INFO			= ASCII_I_LOWER,
	REQUEST_HELP 			= ASCII_H_LOWER
} REQUEST;




DONT_OPTIMIZE_OUT GLOBAL_VARIABLE HANDLE hSocket4, hSocket6;
DONT_OPTIMIZE_OUT GLOBAL_VARIABLE HANDLE hThread4, hThread6;




INTERNAL_OPERATION
LPVOID
__Thread4(
	_In_Opt_ 	LPVOID 		lpParam
){
	HANDLE hClient4, hThisThread;
	IPV4_ADDRESS ip4Address;
	CSTRING csBuffer[0x400];
	CHAR cReceive;

	hClient4 = (HANDLE)lpParam;
	hThisThread = GetCurrentThread();
	ZeroMemory(csBuffer, sizeof(csBuffer));


	INFINITE_LOOP()
	{
		/* Prevent abuse */
		if (8 <= SocketBytesInQueue4(hClient4))
		{ JUMP(__Done4); }

		SocketReceive4(hClient4, (LPVOID)&cReceive, 1);
		switch (cReceive)
		{
			case REQUEST_IPV4_ADDRESS:
			{
				ip4Address = GetSocketIpAddress4(hClient4);
				ConvertIpv4AddressToString(ip4Address, csBuffer, sizeof(csBuffer));
				StringConcatenateSafe(csBuffer, "\n\n", sizeof(csBuffer) - 1);
				SocketSend4(hClient4, csBuffer, StringLength(csBuffer));
				break; 
			}
			case REQUEST_IPV6_ADDRESS:
			{
				LPSTR lpszError = "Please connect on port 6 for IPv6 support.\n\n";
				SocketSend4(hClient4, lpszError, StringLength(lpszError));
				break;
			}
			case REQUEST_CLOSE_CONNECTION:
			{
				JUMP(__Done4);
				break;
			}
			case REQUEST_LICENSE:
			{
				LPSTR lpszResponse = 	"GetPublicIpAddress() is licensed under the GPLv3 License.\n"
							"Check out https://www.gnu.org/licenses/gpl-3.0.en.html for more info.\n\n";
				SocketSend4(hClient4, lpszResponse, StringLength(lpszResponse));
				break;
			}
			case REQUEST_INFO:
			{
				LPSTR lpszResponse = 	"GetPublicIpAddress() was created, and is currently maintained by Zach Podbielniak\n"
							"The source code can be obtained here:\n"
							"\thttps://gitlab.com/zachpodbielniak/GetPublicIpAddress\n\n";
				SocketSend4(hClient4, lpszResponse, StringLength(lpszResponse));
				break;
			}
			case REQUEST_HELP:
			{
				LPSTR lpszResponse = 	"GetPublicIpAddress() Help:\n"
							"\t4 - Gets your current public IPv4 address\n"
							"\t6 - Not supported, connect on port 6\n"
							"\tq - Close the connection\n"
							"\tl - Get license info\n"
							"\ti - Shows info about the project\n"
							"\th - Shows this help screen\n\n";
				SocketSend4(hClient4, lpszResponse, StringLength(lpszResponse));
				break;
			}
		}
	}

	__Done4:
	SocketClose4(hClient4);
	DestroyObject(hClient4);
	DestroyObject(hThisThread);
	return NULLPTR;
}




INTERNAL_OPERATION
LPVOID
__Thread6(
	_In_Opt_ 	LPVOID 		lpParam
){
	HANDLE hClient6, hThisThread;
	LPIPV6_ADDRESS lpip6Address;
	CSTRING csBuffer[0x400];
	CHAR cReceive;

	hClient6 = (HANDLE)lpParam;
	hThisThread = GetCurrentThread();
	ZeroMemory(csBuffer, sizeof(csBuffer));


	INFINITE_LOOP()
	{
		/* Prevent abuse
		if (8 <= SocketBytesInQueue6(hClient6))
		{ JUMP(__Done4); }
		*/

		SocketReceive6(hClient6, (LPVOID)&cReceive, 1);
		switch (cReceive)
		{
			case REQUEST_IPV4_ADDRESS:
			{
				LPSTR lpszError = "Please connect on port 6 for IPv6 support.\n\n";
				SocketSend6(hClient6, lpszError, StringLength(lpszError));
				break; 
			}
			case REQUEST_IPV6_ADDRESS:
			{
				lpip6Address = GetSocketIpv6Address(hClient6);
				ConvertIpv6AddressToCompressedString(lpip6Address, csBuffer, sizeof(csBuffer));
				StringConcatenateSafe(csBuffer, "\n", sizeof(csBuffer));
				SocketSend6(hClient6, csBuffer, StringLength(csBuffer));
				DestroyIpv6Address(lpip6Address);
				break;
			}
			case REQUEST_CLOSE_CONNECTION:
			{
				JUMP(__Done6);
				break;
			}
			case REQUEST_LICENSE:
			{
				LPSTR lpszResponse = 	"GetPublicIpAddress() is licensed under the GPLv3 License.\n"
							"Check out https://www.gnu.org/licenses/gpl-3.0.en.html for more info.\n\n";
				SocketSend6(hClient6, lpszResponse, StringLength(lpszResponse));
				break;
			}
			case REQUEST_INFO:
			{
				LPSTR lpszResponse = 	"GetPublicIpAddress() was created, and is currently maintained by Zach Podbielniak\n"
							"The source code can be obtained here:\n"
							"\thttps://gitlab.com/zachpodbielniak/GetPublicIpAddress\n\n";
				SocketSend6(hClient6, lpszResponse, StringLength(lpszResponse));
				break;
			}
			case REQUEST_HELP:
			{
				LPSTR lpszResponse = 	"GetPublicIpAddress() Help:\n"
							"\t4 - Not supported, connect on port 4\n"
							"\t6 - Gets your current public IPv6 address\n"
							"\tq - Close the connection\n"
							"\tl - Get license info\n"
							"\ti - Shows info about the project\n"
							"\th - Shows this help screen\n\n";
				SocketSend6(hClient6, lpszResponse, StringLength(lpszResponse));
				break;
			}
		}
	}

	__Done6:
	SocketClose6(hClient6);
	DestroyObject(hClient6);
	DestroyObject(hThisThread);
	return NULLPTR;
}




INTERNAL_OPERATION
LPVOID
__4Proc(
	_In_Opt_	LPVOID 		lpParam
){
	HANDLE hClient4;
	CSTRING csBuffer[0x40];
	IPV4_ADDRESS ip4Address;

	hSocket4 = CreateSocket4(SERVER_IPV4_PORT);
	BindOnSocket4(hSocket4);
	ListenOnBoundSocket4(hSocket4, 0x400);

	INFINITE_LOOP()
	{
		hClient4 = AcceptConnection4(hSocket4);

		ip4Address = GetSocketIpAddress4(hClient4);
		ConvertIpv4AddressToString(ip4Address, csBuffer, sizeof(csBuffer));
		StringConcatenateSafe(csBuffer, "\n", sizeof(csBuffer) - 1);

		PrintFormat("Client Connected At: %s", csBuffer);

		CreateThread(__Thread4, hClient4, NULL);

		ZeroMemory(csBuffer, sizeof(csBuffer));
	}

}




INTERNAL_OPERATION
LPVOID
__6Proc(
	_In_Opt_	LPVOID 		lpParam
){
	HANDLE hClient6;
	CSTRING csBuffer[0x40];
	LPIPV6_ADDRESS lpip6Address;

	hSocket6 = CreateSocket6(SERVER_IPV6_PORT);
	BindOnSocket6(hSocket6);
	ListenOnBoundSocket6(hSocket6, 0x400);

	INFINITE_LOOP()
	{
		hClient6 = AcceptConnection6(hSocket6);

		lpip6Address = GetSocketIpv6Address(hClient6);
		ConvertIpv6AddressToCompressedString(lpip6Address, csBuffer, sizeof(csBuffer));
		StringConcatenateSafe(csBuffer, "\n", sizeof(csBuffer) - 1);

		PrintFormat("Client Connected At: %s", csBuffer);

		CreateThread(__Thread6, hClient6, NULL);

		DestroyIpv6Address(lpip6Address);
		ZeroMemory(csBuffer, sizeof(csBuffer));
	}

}




VOID
CreateTelnetCompatibleServer(
	_Out_Opt_	LPHANDLE 		lphThread4,
	_Out_Opt_ 	LPHANDLE 		lphThread6
){
	hThread4 = CreateThread(__4Proc, NULLPTR, 0);
	hThread6 = CreateThread(__6Proc, NULLPTR, 0);

	if (NULLPTR != lphThread4)
	{ *lphThread4 = hThread4; }

	if (NULLPTR != lphThread6)
	{ *lphThread6 = hThread6; }
	
	return;
}