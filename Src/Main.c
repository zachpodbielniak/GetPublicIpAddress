#include <PodNet/PodNet.h>
#include <PodNet/CThread/CThread.h>
#include <PodNet/CNetworking/CSocket.h>
#include <PodNet/CNetworking/CGeo4.h>
#include <CRock/CRock.h>
#include <signal.h>
/* #include "CServer.c" */


#define IPV4_WEB_SITE 		"ipv4life.xyz"
#define IPV4_SERVER_ADDRESS	"198.58.127.106"
#define IPV4_LOCALHOST		"127.0.0.1"




DONT_OPTIMIZE_OUT GLOBAL_VARIABLE HANDLE hSocket4, hSocket6;
DONT_OPTIMIZE_OUT GLOBAL_VARIABLE HANDLE hThread4, hThread6;



#define ADD_GPIA_HEADERS()												\
	RequestAddHeader(lprdData->hRequest, "X-About: https://ipv4life.xyz/about");					\
	RequestAddHeader(lprdData->hRequest, "X-Source: https://gitlab.com/zachpodbielniak/GetPublicIpAddress");	\
	RequestAddHeader(lprdData->hRequest, "X-License: AGPLv3");




INTERNAL_OPERATION
VOID
__SigHandler(
	LONG 	lParam
){
	if (SIGINT == lParam)
	{ PrintFormat("%s\n", "CTRL+C Was Pressed! Shutting Down Server"); }
	else if (SIGSEGV == lParam)
	{ PrintFormat("%s\n", "SEGEMENTATION FAULT! Shutting Down Server"); }

	DestroyObject(hThread4);
	SocketClose4(hSocket4);
	DestroyObject(hSocket4);

	DestroyObject(hThread6);
	SocketClose6(hSocket6);
	DestroyObject(hSocket6);

	exit(0);
}



INTERNAL_OPERATION
BOOL
__IsIpv4(
	_In_ 		LPREQUEST_DATA		lprdData
){
	LPSTR lpszHost;
	lpszHost = lprdData->lpszXRealIp;

	if (
		NULLPTR != StringInString(lpszHost, IPV4_WEB_SITE) || 
		NULLPTR != StringInString(lpszHost, IPV4_SERVER_ADDRESS) || 
		NULLPTR != StringInString(lpszHost, IPV4_LOCALHOST) ||
		NULLPTR == StringInString(lpszHost, ":")
	)
	{ return TRUE; }
	
	return FALSE;
}





_Call_Back_
HTTP_STATUS_CODE
__Root(
	_In_ 		HANDLE			hHttpServer,
	_In_Z_ 		LPCSTR RESTRICT 	lpcszUri,
	_In_ 		HTTP_METHOD 		hmType,
	_In_Opt_ 	LPVOID 			lpUserData,
	_In_ 		LPREQUEST_DATA		lprdData
){
	UNREFERENCED_PARAMETER(hHttpServer);
	UNREFERENCED_PARAMETER(lpcszUri);
	UNREFERENCED_PARAMETER(hmType);
	UNREFERENCED_PARAMETER(lpUserData);

	CSTRING csBuffer[0x100];
	ZeroMemory(csBuffer, sizeof(csBuffer));

	if (NULLPTR != lprdData->lpszXRealIp)
	{ StringPrintFormatSafe(csBuffer, sizeof(csBuffer) - 1, "%s\n", lprdData->lpszXRealIp); }
	else 
	{ 
		if (HANDLE_TYPE_SOCKET == GetHandleDerivativeType(lprdData->hClient))
		{ 
			CSTRING csIpv4[0x20];
			IPV4_ADDRESS ip4Addr;

			ZeroMemory(csIpv4, sizeof(csIpv4));
			ip4Addr = GetSocketIpAddress4(lprdData->hClient);

			ConvertIpv4AddressToString(ip4Addr, csIpv4, sizeof(csIpv4) - 1); 
			StringPrintFormatSafe(csBuffer, sizeof(csBuffer) - 1, "%s\n", csIpv4);
		}
		else 
		{
			CSTRING csIpv6[0x60];
			LPIPV6_ADDRESS lpip6Addr;

			ZeroMemory(csIpv6, sizeof(csIpv6));
			lpip6Addr = GetSocketIpAddress6(lprdData->hClient);

			ConvertIpv6AddressToString(lpip6Addr, csIpv6, sizeof(csIpv6) - 1);
			StringPrintFormatSafe(csBuffer, sizeof(csBuffer) - 1, "%s\n", csIpv6);

			DestroyIpv6Address(lpip6Addr);
		}
	}

	ADD_GPIA_HEADERS();
	RequestAddHeader(lprdData->hRequest, "Content-Type: text/plain");

	RequestAddResponse(lprdData->hRequest, csBuffer, StringLength(csBuffer));
	return HTTP_STATUS_200_OK;
}




_Call_Back_
HTTP_STATUS_CODE
__About(
	_In_ 		HANDLE			hHttpServer,
	_In_Z_ 		LPCSTR RESTRICT 	lpcszUri,
	_In_ 		HTTP_METHOD 		hmType,
	_In_Opt_ 	LPVOID 			lpUserData,
	_In_ 		LPREQUEST_DATA		lprdData
){
	UNREFERENCED_PARAMETER(hHttpServer);
	UNREFERENCED_PARAMETER(lpcszUri);
	UNREFERENCED_PARAMETER(hmType);
	UNREFERENCED_PARAMETER(lpUserData);

	HANDLE hFile;
	LPSTR lpszBuffer, lpszPointer;
	ARCHLONG alRead, alTotalRead;
	
	lpszBuffer = LocalAllocAndZero(sizeof(CHAR) * 0x8000);
	lpszPointer = lpszBuffer;
	alRead = 0;
	alTotalRead = 0;

	hFile = OpenFile("Html/index.html", FILE_PERMISSION_READ, 0);
	if (NULL_OBJECT == hFile)
	{ 
		StringCopySafe(lpszBuffer, "ERROR! Could not open file!\n", sizeof(lpszBuffer));
		JUMP(__ReturnToSender);
	}

	while (0 <= (alRead = ReadLineFromFile(hFile, &lpszPointer, 4096, 0)))
	{
		alTotalRead += alRead;
		lpszPointer = (LPSTR)((UARCHLONG)lpszBuffer + alTotalRead);
	}


	__ReturnToSender:
	ADD_GPIA_HEADERS();
	RequestAddHeader(lprdData->hRequest, "Content-Type: text/html");
	RequestAddResponse(lprdData->hRequest, lpszBuffer, StringLength(lpszBuffer));
	
	FreeMemory(lpszBuffer);
	DestroyObject(hFile);
	return HTTP_STATUS_200_OK;
}




_Call_Back_
HTTP_STATUS_CODE
__Country(
	_In_ 		HANDLE			hHttpServer,
	_In_Z_ 		LPCSTR RESTRICT 	lpcszUri,
	_In_ 		HTTP_METHOD 		hmType,
	_In_Opt_ 	LPVOID 			lpUserData,
	_In_ 		LPREQUEST_DATA		lprdData
){
	UNREFERENCED_PARAMETER(hHttpServer);
	UNREFERENCED_PARAMETER(lpcszUri);
	UNREFERENCED_PARAMETER(hmType);
	UNREFERENCED_PARAMETER(lpUserData);

	LPCSTR lpcszCountry;
	CHAR csBuffer[0x100];
	ZeroMemory(csBuffer, sizeof(csBuffer));

	if (__IsIpv4(lprdData))
	{
		IPV4_ADDRESS ip4Address;
		ip4Address = CreateIpv4AddressFromString((LPCSTR)lprdData->lpszXRealIp);
		lpcszCountry = GetCountryByIpv4Address(ip4Address);
		if (NULLPTR != lpcszCountry)
		{ StringCopySafe(csBuffer, lpcszCountry, sizeof(csBuffer) - 1); }
		else
		{ StringCopySafe(csBuffer, "NO COUNTRY", sizeof(csBuffer) - 1); }
	}
	else
	{ StringCopySafe(csBuffer, "Not implemented in IPv6 yet.", sizeof(csBuffer) - 1); }

	StringConcatenateSafe(csBuffer, "\n", sizeof(csBuffer));
	
	ADD_GPIA_HEADERS();
	RequestAddHeader(lprdData->hRequest, "Content-Type: text/plain");
	RequestAddResponse(lprdData->hRequest, csBuffer, StringLength(csBuffer));

	return HTTP_STATUS_200_OK;
}




_Call_Back_
HTTP_STATUS_CODE
__City(
	_In_ 		HANDLE			hHttpServer,
	_In_Z_ 		LPCSTR RESTRICT 	lpcszUri,
	_In_ 		HTTP_METHOD 		hmType,
	_In_Opt_ 	LPVOID 			lpUserData,
	_In_ 		LPREQUEST_DATA		lprdData
){
	UNREFERENCED_PARAMETER(hHttpServer);
	UNREFERENCED_PARAMETER(lpcszUri);
	UNREFERENCED_PARAMETER(hmType);
	UNREFERENCED_PARAMETER(lpUserData);

	CHAR csBuffer[0x100];
	ZeroMemory(csBuffer, sizeof(csBuffer));

	if (__IsIpv4(lprdData))
	{
		IPV4_ADDRESS ip4Address;
		ip4Address = CreateIpv4AddressFromString((LPCSTR)lprdData->lpszXRealIp);
		GetCityByIpv4Address(ip4Address, csBuffer, sizeof(csBuffer) - 1);
	}
	else
	{ StringCopySafe(csBuffer, "Not implemented in IPv6 yet.", sizeof(csBuffer) - 1); }

	StringConcatenateSafe(csBuffer, "\n", sizeof(csBuffer) - 1);

	ADD_GPIA_HEADERS();
	RequestAddHeader(lprdData->hRequest, "Content-Type: text/plain");
	RequestAddResponse(lprdData->hRequest, csBuffer, StringLength(csBuffer));

	return HTTP_STATUS_200_OK;
}




_Call_Back_
HTTP_STATUS_CODE
__Region(
	_In_ 		HANDLE			hHttpServer,
	_In_Z_ 		LPCSTR RESTRICT 	lpcszUri,
	_In_ 		HTTP_METHOD 		hmType,
	_In_Opt_ 	LPVOID 			lpUserData,
	_In_ 		LPREQUEST_DATA		lprdData
){
	UNREFERENCED_PARAMETER(hHttpServer);
	UNREFERENCED_PARAMETER(lpcszUri);
	UNREFERENCED_PARAMETER(hmType);
	UNREFERENCED_PARAMETER(lpUserData);

	CHAR csBuffer[0x100];
	ZeroMemory(csBuffer, sizeof(csBuffer));

	if (__IsIpv4(lprdData))
	{	
		IPV4_ADDRESS ip4Address;
		LPGEOIPRECORD lpgipRecord;

		ip4Address = CreateIpv4AddressFromString((LPCSTR)lprdData->lpszXRealIp);
		lpgipRecord = GetRecordByIpv4Address(ip4Address);

		StringPrintFormatSafe(
			csBuffer,
			sizeof(csBuffer) - 1,
			"%s\n",
			lpgipRecord->region
		);
	}
	else
	{ StringCopySafe(csBuffer, "Not implemented in IPv6 yet.", sizeof(csBuffer)- 1); }

	StringConcatenateSafe(csBuffer, "\n", sizeof(csBuffer) - 1);
	
	ADD_GPIA_HEADERS();
	RequestAddHeader(lprdData->hRequest, "Content-Type: text/plain");
	RequestAddResponse(lprdData->hRequest, csBuffer, StringLength(csBuffer));

	return HTTP_STATUS_200_OK;
}




_Call_Back_
HTTP_STATUS_CODE
__Coordinates(
	_In_ 		HANDLE			hHttpServer,
	_In_Z_ 		LPCSTR RESTRICT 	lpcszUri,
	_In_ 		HTTP_METHOD 		hmType,
	_In_Opt_ 	LPVOID 			lpUserData,
	_In_ 		LPREQUEST_DATA		lprdData
){
	UNREFERENCED_PARAMETER(hHttpServer);
	UNREFERENCED_PARAMETER(lpcszUri);
	UNREFERENCED_PARAMETER(hmType);
	UNREFERENCED_PARAMETER(lpUserData);

	CHAR csBuffer[0x100];
	ZeroMemory(csBuffer, sizeof(csBuffer));

	if (__IsIpv4(lprdData))
	{	
		IPV4_ADDRESS ip4Address;
		LPGEOIPRECORD lpgipRecord;

		ip4Address = CreateIpv4AddressFromString((LPCSTR)lprdData->lpszXRealIp);
		lpgipRecord = GetRecordByIpv4Address(ip4Address);

		StringPrintFormatSafe(
			csBuffer,
			sizeof(csBuffer) - 1,
			"%f\n%f\n",
			lpgipRecord->latitude,
			lpgipRecord->longitude
		);
	}
	else
	{ StringCopySafe(csBuffer, "Not implemented in IPv6 yet.", sizeof(csBuffer) - 1); }

	StringConcatenateSafe(csBuffer, "\n", sizeof(csBuffer) - 1);
	
	ADD_GPIA_HEADERS();
	RequestAddHeader(lprdData->hRequest, "Content-Type: text/plain");
	RequestAddResponse(lprdData->hRequest, csBuffer, StringLength(csBuffer));

	return HTTP_STATUS_200_OK;
}




_Call_Back_
HTTP_STATUS_CODE
__Verbose(
	_In_ 		HANDLE			hHttpServer,
	_In_Z_ 		LPCSTR RESTRICT 	lpcszUri,
	_In_ 		HTTP_METHOD 		hmType,
	_In_Opt_ 	LPVOID 			lpUserData,
	_In_ 		LPREQUEST_DATA		lprdData
){
	UNREFERENCED_PARAMETER(hHttpServer);
	UNREFERENCED_PARAMETER(lpcszUri);
	UNREFERENCED_PARAMETER(hmType);
	UNREFERENCED_PARAMETER(lpUserData);

	CHAR csBuffer[0x1000];
	ZeroMemory(csBuffer, sizeof(csBuffer));

	if (__IsIpv4(lprdData))
	{	
		IPV4_ADDRESS ip4Address;
		LPGEOIPRECORD lpgipRecord;
		DLPSTR dlpszRange;

		ip4Address = CreateIpv4AddressFromString((LPCSTR)lprdData->lpszXRealIp);
		lpgipRecord = GetRecordByIpv4Address(ip4Address);
		dlpszRange = GetNetRangeByIpv4Address(ip4Address);

		StringPrintFormatSafe(
			csBuffer,
			sizeof(csBuffer) - 1,
			"Area Code: %d\nCity: %s\nContinent: %s\nCountry Code: %s\nCountry Code (3): %s\n" 	\
			"Country: %s\nIPv4: %s\nLatitude: %f\nLongitude: %f\nPostal Code: %s\n" 		\
			"Range: %s - %s\nRange (CIDR): %s/%d\nRegion: %s\n",
			lpgipRecord->area_code,
			lpgipRecord->city,
			lpgipRecord->continent_code,
			lpgipRecord->country_code,
			lpgipRecord->country_code3,
			lpgipRecord->country_name,
			lprdData->lpszXRealIp,
			lpgipRecord->latitude,
			lpgipRecord->longitude,
			lpgipRecord->postal_code,
			dlpszRange[0],
			dlpszRange[1],
			lprdData->lpszXRealIp,
			lpgipRecord->netmask,
			lpgipRecord->region
		);

		DestroyIpv4Record(lpgipRecord);
	}
	else
	{ StringCopySafe(csBuffer, "Not implemented in IPv6 yet.", sizeof(csBuffer) - 1); }

	StringConcatenateSafe(csBuffer, "\n", sizeof(csBuffer) - 1);
	
	ADD_GPIA_HEADERS();
	RequestAddHeader(lprdData->hRequest, "Content-Type: text/plain");
	RequestAddResponse(lprdData->hRequest, csBuffer, StringLength(csBuffer));

	return HTTP_STATUS_200_OK;
}




LONG
Main(
	_In_		LONG 			lArgCount,
	_In_Z_ 		DLPSTR 			dlpszArgValues
){
	UNREFERENCED_PARAMETER(lArgCount);
	UNREFERENCED_PARAMETER(dlpszArgValues);

	signal(SIGINT, __SigHandler);
	signal(SIGSEGV, __SigHandler);

	HANDLE hHttpServer;
	DLPSTR dlpszBindAddresses;
	LPUSHORT lpusBindPorts;

	dlpszBindAddresses = GlobalAllocAndZero(sizeof(LPSTR));
	lpusBindPorts = GlobalAllocAndZero(sizeof(USHORT));

	dlpszBindAddresses[0] = "0.0.0.0";
	lpusBindPorts[0] = 9998;

	hHttpServer = CreateHttpServerEx(
		"GetPublicIpAddress",
		(DLPCSTR)dlpszBindAddresses,
		1,
		lpusBindPorts,
		1,
		"getpublicipaddress.local",
		65536,
		4,
		0
	);

	DefineHttpUriProc(
		hHttpServer,
		"/",
		HTTP_METHOD_GET,
		__Root,
		NULLPTR
	);

	DefineHttpUriProc(
		hHttpServer,
		"/about",
		HTTP_METHOD_GET,
		__About,
		NULLPTR
	);
	
	DefineHttpUriProc(
		hHttpServer,
		"/country",
		HTTP_METHOD_GET,
		__Country,
		NULLPTR
	);
	
	DefineHttpUriProc(
		hHttpServer,
		"/region",
		HTTP_METHOD_GET,
		__Region,
		NULLPTR
	);
	
	DefineHttpUriProc(
		hHttpServer,
		"/coordinates",
		HTTP_METHOD_GET,
		__Coordinates,
		NULLPTR
	);
	
	DefineHttpUriProc(
		hHttpServer,
		"/verbose",
		HTTP_METHOD_GET,
		__Verbose,
		NULLPTR
	);

	/*
	CreateTelnetCompatibleServer(
		&hThread4,
		&hThread6
	);

	WaitForSingleObject(hThread4, INFINITE_WAIT_TIME);
	WaitForSingleObject(hThread6, INFINITE_WAIT_TIME);
	*/
	
	INFINITE_LOOP()
	{ LongSleep(INFINITE_WAIT_TIME); }

	return 0;
}